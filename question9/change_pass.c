#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]) {
	char bufferPass[80];
	char bufferNewPass[80];
	char *user = getenv("USER");
	char *getUser;
	char *getMdp;
	char *string;
	char *array[1024];	
	
	FILE *file  = fopen("/home/admin/passwd", "r"); 
           
	// test for file not existing. 
	if (file == NULL) {   
		printf("Error! Could not open file\n"); 
		exit(-1); // must include stdlib.h 
	} 
	
	for(int i=0; i<1024; i++) {
		array[i] = (char *)malloc(1024);
		char *p = fgets(array[i], 1024, file);
		if (p == NULL) break;
	}
	fclose(file);
	
		
	FILE *in_file  = fopen("/home/admin/passwd", "w"); 
	for(int i=0; i<1024; i++) {
		if (array[i] == NULL) break;
		//printf("ligne %d: %s\n", i, array[i]);
		
		string = strdup(array[i]);
		getUser = strsep(&string, ":");
		//printf("getUser: %s\n", getUser);
		getMdp = strsep(&string, ":");
		//printf("getMdp: %s\n", getMdp);
		
		if(strcmp(user, getUser) == 0) {
			printf( "Veuillez saisir votre ancien mot de passe: " );
			fflush( stdout );
			scanf( "%[^\n]", bufferPass);
			getchar();
			//il faut rajouter le \n parce qu'il est pris dans getMdp
			strcat(bufferPass, "\n");
			
			
			if(strcmp(bufferPass, getMdp) == 0) {
				printf("Vous avez bien tapé votre mot de passe.\n");
				
				
				printf( "Veuillez saisir votre nouveau mot de passe: " );
				fflush( stdout );
				scanf( "%[^\n]", bufferNewPass);
				getchar();
				printf("nouveau mdp: %s\n", bufferNewPass);
				
				
				
				printf("Votre mot de passe a bien été changé.\n");
				strcat(getUser, ":");
				strcat(getUser, bufferNewPass); 
				strcat(getUser, "\n"); 
				fputs(getUser, in_file);
			} else {
				printf("Erreur: vous n'avez pas bien tapé votre ancien mot de passe.\n");
				
				fputs(array[i], in_file);
			}
		} else {
			fputs(array[i], in_file);
		}
	}
 
	fclose(in_file);
	return 0;
}


          
