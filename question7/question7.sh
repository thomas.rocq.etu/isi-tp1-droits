#!/bin/bash

#vérification de lecture
cd dir_a && cd .. 
cat dir_a/texteA.txt

cd dir_b && cd ..  
cat dir_b/texteB.txt 
 
cd dir_c && cd .. 
cat dir_c/texteC.txt 

#vérication d'écriture
mkdir dir_a/test 
rmdir dir_a/test 

mkdir dir_b/test 
rmdir dir_b/test 

mkdir dir_c/test 
rmdir dir_c/test 
