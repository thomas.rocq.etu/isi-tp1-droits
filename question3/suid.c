#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

 int main () {
  int uid = getuid();
  int gid = getgid();
  int euid = geteuid();
  int egid = getegid();

  printf("uid : %d\ngid : %d\neuid : %d\negid : %d\n", uid, gid, euid, egid);

  FILE *f  = fopen("./mydir/data.txt", "r");
  if (f == NULL) {
        printf("Error, could not open file \n");
        exit(-1);
  }

  char c;

  c = fgetc(f);
    while (c != EOF)
    {
        printf ("%c", c);
        c = fgetc(f);
    }

    fclose(f);
    return 0;
}