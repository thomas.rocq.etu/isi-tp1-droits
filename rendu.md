# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- ROCQ, Thomas, email: thomas.rocq.etu@univ-lille.fr

- SOUVANTHONG, Donovan, email: donovan.souvanthong.etu@univ-lille.fr

## Question 1

Tous les membres du groupe ubuntu peuvent écrire sauf toto car il n'a accès au fichier qu'en lecture.

## Question 2

- Le caractère x pour un répertoire permet l'accès au répertoire.
- L'accès à mydir avec l'utilisateur toto ne fonctionne pas car on a retiré les droits d'exécution pour le group ubuntu, et toto est dans ce groupe donc il n'a accès au fichier qu'en lecture et écriture.
- On peut voir le contenu du dossier mydir mais sans les droits associés aux fichiers car toto n'a pas les droits executables sur mydir.

## Question 3

- Sans le set-user-id: 
uid : 1001
gid : 1000
euid : 1001
egid : 1000

Le processus ne peut pas ouvrir mydir/mydata.txt en lecture.

- Avec le set-user-id:
uid : 1001
gid : 1000
euid : 1000
egid : 1000

Le processus arrive à ouvrir mydir/mydata.txt en lecture.

## Question 4

Réponse:
En passant par un script appartenant à root et ayant le set-user-id d'activé.

## Question 5

Réponse: chfn modifie le nom complet et les informations associées à un utilisateur
-rwsr-xr-x 1 root root 76496 jav. 25 2018 /usr/bin/chfn

- chfn permet de changer les informations de l'utilisateur.
Le s permet de l'executer  avec les permissions du propriétaire du programme.
Le r permet de lire le fichier
Le w permet d'écrire le fichier

Il faut faire attention car 
1) -    -> Permet de dire s'il s'agit d'un répertoire ou non
2) rws  -> liste des permissions pour l'utilisateur courant
3) r-x  -> liste des permissions du groupe
4) r-x  -> liste des persmissions pour les autres
Le fichier passwd à été modifié, on a maitennant les nouvelles informations misent à la suite du nom de l'utilisateur.

## Question 6

Réponse: Les mots de passe sont stockés dans /etc/shadow et ne peuvent être accédé que par root en écriture et lecture

## Question 7

Mettre les scripts bash dans le repertoire *question7*.
Les dossiers ont été créer dans le répertoire /travail/
Tout les utilisateurs peuvent accèder a /travail/
Commande:
```
useradd lambda_a
adduser lambda_a groupe_a


useradd lambda_a2
adduser lambda_a2 groupe_a


useradd lambda_b
adduser lambda_b groupe_b

useradd admin
adduser admin groupe_a
adduser admin groupe_b

mkdir dir_a
mkdir dir_b
mkdir dir_c

chgrp -R groupe_a dir_a
chmod -R 2770 dir_a/

chgrp -R groupe_b dir_b
chmod -R 2770 dir_b/

chmod -R 2705 dir_c/
```

## Question 8

Le programme et les scripts dans le repertoire *question8*.
Commande:
```
make check_pass
sudo chown admin:admin check_pass 
sudo chmod +s check_pass
```
Le script question8.sh doit avoir le droit en exécution pour tout le monde.

## Question 9

Le programme et les scripts dans le repertoire *question9*.
PAS FINI
Commande:
```
make change_pass
sudo chown admin:admin change_pass 
sudo chmod +s change_pass
```
Le script question9.sh doit avoir le droit en exécution pour tout le monde.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 
QUESTION ABANDONNEE








